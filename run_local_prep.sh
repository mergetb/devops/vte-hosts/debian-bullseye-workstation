#!/bin/bash

set -euo pipefail

#
# Use the optional extra vars file if its there
#

extra_vars_path="extra_vars/local_prep.yml"
if [[ -e "${extra_vars_path}" ]]
then
    extra_vars_opt="--extra-vars=\"\@{${extra_vars_path}}\""
else
    extra_vars_opt="--extra-vars=''"
fi

#
# Invoke the playbook
#

echo "Info: Invoking the ansible playbook"

ansible-playbook -vv \
		 -i "127.0.0.1," \
		 "${extra_vars_opt}" \
		 ansible/local_prep.yml

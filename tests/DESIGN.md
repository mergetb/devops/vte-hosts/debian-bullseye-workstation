# Design Notes for Tests

## VTE installation test

### Overview

Mix of automated and manual steps.

The automated parts are orchestrated via Ansible playbooks which target the remote server on which the installation
should take place. The manual parts have to be done while logged in on the remote server either via ssh or GUI.

Due to the nature of the `run.sh`, right now all the ansible playbooks automate is the initial setup of a guinea pig
git clone etc. in preparation for a manual execution of the `run.sh`

### Assumptions and Caveats

* The scope of the "test" is a crude smoke test of the ability to install at all.
  * It **does not** attempt to introspect on the VTE after installation.
* It is assumed that the non-root user that Ansible logs in as, is the developer oriented user we want to empower for vte dev work
* As the VTE installation process is a bit of a moving target and may sometimes need manual intervention, the only
  currently automated thing is the setup in preparation for a manual test.
* The test setup stages the vte working area in a specific sub-directory under the non-root user's home
  * Due to the need for Raven to run as root, this sub-directory will be tainted by root ownership

### Setup Workflow

The test setup workflow (encapsulated in the `tests/ansible/setup.yml` playbook) is as follows:

1. Setup a working directory in the home of the non-root "developer" user on the remote box: `/home/{user}/prov_test`
2. Do a git clone/update to stage a copy of the VTE repository at `/home/{user}/prov_test/vte`
3. Update the permissions on the `/home/{user}/vte_host_prov_test` directory (and its contents) to accomodate the way that Raven
   expects to be run as `root`.

# Contributing

## Testing

### Full Testing

1. Get a Colfax box
2. Follow the [usage docs to set it up](USAGE.md)
3. Then run an ansible playbook to do initial setup for a test VTE install
   ```shell
   cd <repo>
   ./setup_vte_install_test.sh {host|ip} {user}
   ```
4. Login via ssh etc. onto remote box, and invoke the `run.sh` manually:
   ```shell
   ssh <remote_box>
   cd /home/{user}/prov_test/vte/integrated
   sudo su
   ./run.sh
   ```

Please see notes under `tests/` for details about the design of the above test(s).

# Usage

## Overview

Given an Ansible host machine and a destination machine (the machine being provisioned), do the following:

1. Perform some manual steps to prepare the Ansible host machine before running Ansible from it
2. Perform some manual steps on the destination machine to prepare it for provisioning via Ansible
3. Run an Ansible playbook on the Ansible host machine, which will remotely provision the destination machine
   by running custom Ansible roles etc.
4. Perform some manual steps afterwards to finish out the provisioning

## Assumptions

### Machine being provisioned

* Colfax or similar machine is the destination

### Machine hosting the Ansible recipe invocation

* Linux like OS
* The following software installed
  * Ansible (version 2.9.15)
  * bash
  * curl
  * git
  * Python (version 3.8+)
  * ssh client
* Optional custom Ansible extra vars files
  * If you need to override variables used in the Ansible playbooks in this repository, then you can stage overridding
	'extra vars' YAML files in the `extra_vars/` directory and they will be ignored by git, and the bash scripts which
	run the Ansible playbooks will look for them there.
  * If these files don't exist, then the bash+Ansible will gracefully continue, so you don't need to put placeholders here.

### Networking

* The destination machine is network accessible to the Ansible host machine.
* Both have access to the public internet, but are not themselves visible on the public internet

---

## Pre-Ansible steps

1. Install debian bullseye alpha 2
  * Download the [debian-bullseye-DI-alpha2-amd64-DVD-1.iso](https://cdimage.debian.org/cdimage/bullseye_di_alpha2/amd64/iso-dvd/debian-bullseye-DI-alpha2-amd64-DVD-1.iso) from the official iso image site: https://cdimage.debian.org/cdimage/bullseye_di_alpha2/amd64/iso-dvd/
  * Make sure to install the openssh server
  * Optionally, you can install the desktop windows manager etc. of your choice.  The ansible will always make sure that at least a headless XServer is available. 
  * Setup a non-root user who has sudo privileges.  The examples in the documentation all assume a username of `developer`.
2. Install these things via apt: `apt install sudo python3 python3-dev python3-apt`
3. Make sure the non-root user has sudo privileges
4. Bounce the box

---

## Ansible-based steps

### Prepare your local environment

Run this shell script to invoke an Ansible playbook that prepares your local working clone of this repository with
additional assets needed before invoking the other Ansible playbooks.

```shell
cd <repo>
./run_local_prep.sh
```

Some caveats/additional notes:

* If you want to tailor the behavior of the local prep stuff, then create an 'extra vars' file
  called `extra_vars/local_prep.yml` with new definitions of the variables in `ansible/vars/local_prep.yml`.

### Provisioning the destination machine

Run this shell script to invoke an Ansible playbook that will provision the destination machine:

```shell
cd <repo>
./run_provisioning.sh {host|ip} {username}
```

In the above example:

* The `{host|ip}` is the host name or ip address of the machine being provisioned
* The `{username}` is the name of the non-root user with sudo privileges

---

## Post-Ansible steps

At this stage, the user is responsible for doing the actual VTE installation process.

A reference "implementation" of that has been implemented under `tests/`.  Please see the [test design doc](tests/DESIGN.md) for more information.

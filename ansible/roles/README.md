A custom invocation of ansible-galaxy using specific git urls etc. will be used to stage in-house custom roles here.

These are roles that are not being distributed by Ansible's cloud galaxy repositories etc.

None should be commited to this repository.  Please see the [USAGE doc](../USAGE.md) for more details on the roles being used.

# Design

## Purpose

This is a reference implementation of Ansible-based tooling for provisioning a "legacy" Debian Bullseye workstation using a Colfax (or similar) machine.

It will use in-house Ansible roles for cross-cutting concerns, and supplement them with local logic specific to the workstation scenario.

Note that this will not automate the VTE install itself, just the prep of the VTE host for that installation.

## Overview

It is assumed that some basic manual steps have to be performed on a fresh machine before invoking an Ansible workflow.

Please see the [usage doc](USAGE.md) for details on those steps.

After those pre-requisites are taken care of, the Ansible workflow will be invoked to do the following:

>
> TODO: Details forthcoming
>

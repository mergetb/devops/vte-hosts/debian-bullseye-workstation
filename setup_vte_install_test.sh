#!/bin/bash

set -eo pipefail

#
# Parse command line arg(s)
#

if [[ -z "${1}" ]]
then
    echo "Error: First cli arg, the hostname/ip of the target machine, was missing or empty string"
    exit 1
else
    target_host="${1}"
fi

if [[ -z "${2}" ]]
then
    echo "Error: Second cli arg, the username on the target machine, was missing or empty string"
    exit 1
else
    user_name="${2}"
fi

set -u

#
# Use the optional extra vars file if its there
#

extra_vars_path="extra_vars/setup.yml"
if [[ -e "${extra_vars_path}" ]]
then
    extra_vars_opt="--extra-vars=@${extra_vars_path}"
else
    extra_vars_opt="--extra-vars=''"
fi

echo "Info: extra_vars_opt='${extra_vars_opt}'"

#
# Invoke the playbook
#

echo "Info: Invoking the ansible playbook"

cd tests
export ANSIBLE_LOG_PATH=../logs/setup_vte_install_test_ansible.log
export ANSIBLE_CONFIG=./ansible.cfg
ansible-playbook -vv \
		 -i "${target_host}," \
		 "${extra_vars_opt}" \
		 -u "${user_name}" \
		 --ask-pass \
		 --ask-become-pass \
		 ansible/setup.yml

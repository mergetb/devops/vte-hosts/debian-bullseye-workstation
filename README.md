# Provisioning Recipe for Debian Bullseye Workstation

This is a reference implementation of a set of Ansible recipes for provisioning a Debian Bullseye based VTE host in
preparation for the installation of the actual VTE (using the source in mergetb/vte).

This recipe assumes the following:

* Starting with a fresh install of Debian Bullseye
* Colfax or similar machine

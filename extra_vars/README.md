This directory is used to contain optional Ansible "extra vars" files that the bash scripts which invoke Ansible will look for by default.

Please see the [USAGE](../USAGE.md) guide for more details.
